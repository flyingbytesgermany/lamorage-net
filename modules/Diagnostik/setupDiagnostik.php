<?php
//include_once 'vtlib/Vtiger/Module.php';
//$Vtiger_Utils_Log = true;
//
//$MODULENAME = 'MyRss';
//
//$moduleInstance = Vtiger_Module::getInstance($MODULENAME);
//if ($moduleInstance || file_exists('modules/'.$MODULENAME)) {
//echo "Module already present - choose a different name.";
//} else {
//$moduleInstance = new Vtiger_Module();
//$moduleInstance->name = $MODULENAME;
//$moduleInstance->parent= 'Tools';
//$moduleInstance->save();
//
//mkdir('modules/'.$MODULENAME);
//echo "OK\n";
//}



include_once 'vtlib/Vtiger/Module.php';

$Vtiger_Utils_Log = true;

$MODULENAME = 'Diagnostik';

$moduleInstance = Vtiger_Module::getInstance($MODULENAME);
//if ($moduleInstance || file_exists('modules/'.$MODULENAME)) {
if ($moduleInstance) {
//	echo "Module already present - choose a different name.";
	$moduleInstance->delete();
	echo "Module already present - deleted.";
} else {

	$moduleInstance = new Vtiger_Module();
	$moduleInstance->name = $MODULENAME;
	$moduleInstance->parent= 'Tools';
	$moduleInstance->save();

	// Schema Setup
	$moduleInstance->initTables();

	// Field Setup
	$block = new Vtiger_Block();
	$block->label = 'LBL_'. strtoupper($moduleInstance->name) . '_INFORMATION';
	$moduleInstance->addBlock($block);

	$blockcf = new Vtiger_Block();
	$blockcf->label = 'LBL_'. strtoupper($moduleInstance->name) . '_MEASUREMENTS';
	$moduleInstance->addBlock($blockcf);

	$field1  = new Vtiger_Field();
	$field1->name = 'title';
	$field1->label= 'Title';
	$field1->uitype= 2;
	$field1->column = $field1->name;
	$field1->columntype = 'VARCHAR(255)';
	$field1->typeofdata = 'V~M';
	$field1->quickcreate = 0;
	$block->addField($field1);

	$moduleInstance->setEntityIdentifier($field1);

	$field2  = new Vtiger_Field();
	$field2->name = 'date';
	$field2->label= 'Date';
	$field2->uitype= 5;
	$field2->column = $field2->name;
	$field2->columntype = 'Date';
	$field2->typeofdata = 'D~M';
	$field2->quickcreate = 0;
	$block->addField($field2);

	$field3  = new Vtiger_Field();
	$field3->name = 'description';
	$field3->label= 'Description';
	$field3->uitype= 21;
	$field3->column = $field3->name;
	$field3->columntype = 'TEXT';
	$field3->typeofdata = 'V~O';
	$field3->quickcreate = 0;
	$block->addField($field3);

	$field4  = new Vtiger_Field();
	$field4->name = 'dval1';
	$field4->label= 'dval1';
	$field4->uitype= 7;
	$field4->displaytype= 2;
	$field4->column = $field4->name;
	$field4->columntype = 'DECIMAL(11,2)';
	$field4->typeofdata = 'NN~O~7,2';
	$field4->quickcreate = 0;
	$blockcf->addField($field4);

	$field5  = new Vtiger_Field();
	$field5->name = 'dval2';
	$field5->label= 'dval2';
	$field5->uitype= 7;
	$field5->displaytype= 2;
	$field5->column = $field5->name;
	$field5->columntype = 'DECIMAL(11,2)';
	$field5->typeofdata = 'NN~O~7,2';
	$field5->quickcreate = 0;
	$blockcf->addField($field5);

	$field7  = new Vtiger_Field();
	$field7->name = 'dval3';
	$field7->label= 'dval3';
	$field7->uitype= 7;
	$field7->displaytype= 2;
	$field7->column = $field7->name;
	$field7->columntype = 'DECIMAL(11,2)';
	$field7->typeofdata = 'NN~O~7,2';
	$field7->quickcreate = 0;
	$blockcf->addField($field7);

	$field8  = new Vtiger_Field();
	$field8->name = 'dval4';
	$field8->label= 'dval4';
	$field8->uitype= 7;
	$field8->displaytype= 2;
	$field8->column = $field8->name;
	$field8->columntype = 'DECIMAL(11,2)';
	$field8->typeofdata = 'NN~O~7,2';
	$field8->quickcreate = 0;
	$blockcf->addField($field8);

	$field9  = new Vtiger_Field();
	$field9->name = 'dval5';
	$field9->label= 'dval5';
	$field9->uitype= 7;
	$field9->displaytype= 2;
	$field9->column = $field9->name;
	$field9->columntype = 'DECIMAL(11,2)';
	$field9->typeofdata = 'NN~O~7,2';
	$field9->quickcreate = 0;
	$blockcf->addField($field9);

	$field10  = new Vtiger_Field();
	$field10->name = 'dval6';
	$field10->label= 'dval6';
	$field10->uitype= 7;
	$field10->displaytype= 2;
	$field10->column = $field10->name;
	$field10->columntype = 'DECIMAL(11,2)';
	$field10->typeofdata = 'NN~O~7,2';
	$field10->quickcreate = 0;
	$blockcf->addField($field10);

	$field11  = new Vtiger_Field();
	$field11->name = 'dval7';
	$field11->label= 'dval7';
	$field11->uitype= 7;
	$field11->displaytype= 2;
	$field11->column = $field11->name;
	$field11->columntype = 'DECIMAL(11,2)';
	$field11->typeofdata = 'NN~O~7,2';
	$field11->quickcreate = 0;
	$blockcf->addField($field11);

	$field12  = new Vtiger_Field();
	$field12->name = 'dval8';
	$field12->label= 'dval8';
	$field12->uitype= 7;
	$field12->displaytype= 2;
	$field12->column = $field12->name;
	$field12->columntype = 'DECIMAL(11,2)';
	$field12->typeofdata = 'NN~O~7,2';
	$field12->quickcreate = 0;
	$blockcf->addField($field12);

	$field13  = new Vtiger_Field();
	$field13->name = 'ival1';
	$field13->label= 'ival1';
	$field13->uitype= 7;
	$field13->displaytype= 2;
	$field13->column = $field13->name;
	$field13->columntype = 'DECIMAL(11,2)';
	$field13->typeofdata = 'NN~O~8,2';
	$field13->quickcreate = 0;
	$blockcf->addField($field13);

	$field13_1  = new Vtiger_Field();
	$field13_1->name = 'ival1_image';
	$field13_1->label= 'ival1_image';
	$field13_1->uitype= 28;
	$field13_1->displaytype= 2;
	$field13_1->column = $field13_1->name;
	$field13_1->columntype = 'TEXT';
	$field13_1->typeofdata = 'V~O';
	$field13_1->quickcreate = 0;
	$blockcf->addField($field13_1);

	$field14  = new Vtiger_Field();
	$field14->name = 'ival2';
	$field14->label= 'ival2';
	$field14->uitype= 7;
	$field14->displaytype= 2;
	$field14->column = $field14->name;
	$field14->columntype = 'DECIMAL(11,2)';
	$field14->typeofdata = 'NN~O~8,2';
	$field14->quickcreate = 0;
	$blockcf->addField($field14);

	$field14_1  = new Vtiger_Field();
	$field14_1->name = 'ival2_image';
	$field14_1->label= 'ival2_image';
	$field14_1->uitype= 28;
	$field14_1->displaytype= 2;
	$field14_1->column = $field14_1->name;
	$field14_1->columntype = 'TEXT';
	$field14_1->typeofdata = 'V~O';
	$field14_1->quickcreate = 0;
	$blockcf->addField($field14_1);

	$field15  = new Vtiger_Field();
	$field15->name = 'ival3';
	$field15->label= 'ival3';
	$field15->uitype= 7;
	$field15->displaytype= 2;
	$field15->column = $field15->name;
	$field15->columntype = 'DECIMAL(11,2)';
	$field15->typeofdata = 'NN~O~8,2';
	$field15->quickcreate = 0;
	$blockcf->addField($field15);

	$field15_1  = new Vtiger_Field();
	$field15_1->name = 'ival3_image';
	$field15_1->label= 'ival3_image';
	$field15_1->uitype= 28;
	$field15_1->displaytype= 2;
	$field15_1->column = $field15_1->name;
	$field15_1->columntype = 'TEXT';
	$field15_1->typeofdata = 'V~O';
	$field15_1->quickcreate = 0;
	$blockcf->addField($field15_1);

	$field16  = new Vtiger_Field();
	$field16->name = 'ival4';
	$field16->label= 'ival4';
	$field16->uitype= 7;
	$field16->displaytype= 2;
	$field16->column = $field16->name;
	$field16->columntype = 'DECIMAL(11,2)';
	$field16->typeofdata = 'NN~O~8,2';
	$field16->quickcreate = 0;
	$blockcf->addField($field16);

	$field16_1  = new Vtiger_Field();
	$field16_1->name = 'ival4_image';
	$field16_1->label= 'ival4_image';
	$field16_1->uitype= 28;
	$field16_1->displaytype= 2;
	$field16_1->column = $field16_1->name;
	$field16_1->columntype = 'TEXT';
	$field16_1->typeofdata = 'V~O';
	$field16_1->quickcreate = 0;
	$blockcf->addField($field16_1);

	// Recommended common fields every Entity module should have (linked to core table)
	$mfield1 = new Vtiger_Field();
	$mfield1->name = 'assigned_user_id';
	$mfield1->label = 'Assigned To';
	$mfield1->table = 'vtiger_crmentity';
	$mfield1->column = 'smownerid';
	$mfield1->uitype = 53;
	$mfield1->typeofdata = 'V~M';
	$block->addField($mfield1);

	$mfield11 = new Vtiger_Field();
	$mfield11->name = 'assigned_contact_id';
	$mfield11->label = 'Contact';
	$mfield11->uitype = 10;
//	$mfield11->table = 'vtiger_crmentityrel';
	$mfield11->column = 'contactid';
	$mfield11->typeofdata = 'V~O';
	$mfield11->displaytype= 2;
	$block->addField($mfield11);
	$mfield11->setRelatedModules(Array('Contacts'));

	$mfield2 = new Vtiger_Field();
	$mfield2->name = 'CreatedTime';
	$mfield2->label= 'Created Time';
	$mfield2->table = 'vtiger_crmentity';
	$mfield2->column = 'createdtime';
	$mfield2->uitype = 70;
	$mfield2->typeofdata = 'T~O';
	$mfield2->displaytype= 2;
	$block->addField($mfield2);

	$mfield3 = new Vtiger_Field();
	$mfield3->name = 'ModifiedTime';
	$mfield3->label= 'Modified Time';
	$mfield3->table = 'vtiger_crmentity';
	$mfield3->column = 'modifiedtime';
	$mfield3->uitype = 70;
	$mfield3->typeofdata = 'T~O';
	$mfield3->displaytype= 2;
	$block->addField($mfield3);

	// Filter Setup
	$filter1 = new Vtiger_Filter();
	$filter1->name = 'All';
	$filter1->isdefault = true;
	$moduleInstance->addFilter($filter1);
	$filter1->addField($field1)->addField($field2, 1)->addField($field3, 2)->addField($mfield1, 3);

	// Sharing Access Setup
	$moduleInstance->setDefaultSharing();

	// Webservice Setup
	$moduleInstance->initWebservice();

//	$documentsModule = Vtiger_Module::getInstance('Documents');
//	$relationLabel  = 'BeautySession Documents';
//	$moduleInstance->setRelatedList(
//		$documentsModule, $relationLabel, Array('ADD','SELECT'),'get_attachments'
//	);

	$contactsInstance = Vtiger_Module::getInstance('Contacts');
	$relationLabel  = 'Diagnostik';
	$contactsInstance->setRelatedList(
		$moduleInstance, $relationLabel, Array('ADD','SELECT')
	);

	if(!file_exists('modules/'.$MODULENAME)) {
		mkdir('modules/'.$MODULENAME);
	}

	echo "OK\n";
}