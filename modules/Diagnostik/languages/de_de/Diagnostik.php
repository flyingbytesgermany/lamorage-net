<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
$languageStrings = array(
	'Title'							=> 'Bezeichnung',
	'SINGLE_Diagnostik'				=> 'Messung',
	'LBL_DIAGNOSTIK_INFORMATION'	=> 'Informationen',
	'LBL_DIAGNOSTIK_MEASUREMENTS'	=> 'Messungen',
	'Diagnostik'					=> 'Diagnostik',
	'Date'							=> 'Datum',
	'dval1'							=> 'Feuchtigkeit',
	'dval2'							=> 'Elastizität',
	'dval3'							=> 'Hautfett (Stirn)',
	'dval4'							=> 'Hautfett (Wange)',
	'dval5'							=> 'dval5',
	'dval6'							=> 'dval6',
	'dval7'							=> 'dval7',
	'dval8'							=> 'dval8',
	'ival1'							=> 'Poren',
	'ival1_image'					=> 'Porenan-Alyse',
	'ival2'							=> 'Flecken',
	'ival2_image'					=> 'Flecken-Analyse',
	'ival3'							=> 'Fältchen',
	'ival3_image'					=> 'Fältchen-Analyse',
	'ival4'							=> 'ival4',
	'ival4_image'					=> 'ival4-Analyse',
	'LBL_PRINT'						=> 'PDF generieren und herunterladen',
);
$jsLanguageStrings = array(
);